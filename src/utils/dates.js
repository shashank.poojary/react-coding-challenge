
import moment from 'moment';

moment.locale('en');

export const getWeeksOfMonth =  date => {
	const startOfMonth = moment(date).startOf('month');
	const endOfMonth = moment(date).endOf('month');
	const weeks = [];
	while (moment(startOfMonth).startOf('isoWeek') <= endOfMonth) {
		const week = [];
		const startOfWeek = moment(startOfMonth).startOf('isoWeek');
		const endOfWeek = moment(startOfMonth).endOf('isoWeek');
		while (startOfWeek <= endOfWeek) {
			week.push(moment(startOfWeek));
			startOfWeek.add(1,'day');
		}
		weeks.push(week);
		startOfMonth.add(1,'week');
	}
	return weeks;
};


export const isDateDisabled = (date, checkDate) => {
	const startOfMonth = moment(date).startOf('month');
	const endOfMonth = moment(date).endOf('month');
	if (checkDate >= startOfMonth && checkDate <= endOfMonth) {
		return false;
	}
	return true;
};