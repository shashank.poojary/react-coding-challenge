import moment  from 'moment';

export const fliterByWeek = (reminders, week) => {
	const filteredReminders = reminders.filter(reminder => 
		moment(reminder.date) >= week[0] && moment(reminder.date) <= moment(week[week.length - 1]).endOf('day')
	);
	return filteredReminders;
};

export const filterByDay = (reminders, day) => {
	const start = moment(day).startOf('day');
	const end = moment(day).endOf('day');
	const filteredReminders = reminders.filter(reminder => 
		moment(reminder.date) >= start && moment(reminder.date) <= end
	);	
	return filteredReminders;
};