export const sortByDateTime = (arr, dateProp) => {
	return arr.slice().sort(function (a, b) {
		return a[dateProp] < b[dateProp] ? -1 : 1;
	});
};