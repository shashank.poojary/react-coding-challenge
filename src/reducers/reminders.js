import { ADD_REMINDER, EDIT_REMINDER, DELETE_REMINDER } from '../constants/actionTypes';

const initialState = [];

const reminders = (state = initialState, action) => {
	switch (action.type) {
	case ADD_REMINDER: {
		const {reminder} = action;
		const newState = Object.assign([], state);
		reminder.id = state.length + 1;
		newState.push(reminder);
		return newState;
	}
	case EDIT_REMINDER: {
		const {reminder} = action;
		const index = state.findIndex(data => data.id === reminder.id );
		if (index < 0) {
			return state;
		}
		const newState = Object.assign([], state);
		newState[index] = reminder;
		return newState;
	}
	case DELETE_REMINDER: {
		const {id} = action;
		const index = state.findIndex(data => data.id === id );
		if (index < 0) {
			return state;
		}
		const newState = Object.assign([], state);
		newState.splice(index, 1);
		return newState;
	}
	default:
		return state;
	}
};
export default reminders;