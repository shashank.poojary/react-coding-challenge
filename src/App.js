import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './App.css';
import Calendar from './components/Calendar';
import { addReminder, editReminder, deleteReminder } from './actions/reminder';
import moment from 'moment';


class App extends Component {
	render () {
		const date = moment();
		const {props} = this;

		return (
			<div>
				<main>
					<Calendar 
						date={date}
						addReminder={props.addReminder}
						editReminder={props.editReminder}
						reminders={props.reminders}
						onDelete={props.deleteReminder}
					/>
				</main>
			</div>
		);
	}
}


const mapStateToProps = state => {
	const {reminders} = state;
	return { 
		reminders 
	};
};
  

const mapDispatchToProps = dispatch => {
	return {
		addReminder: reminder => dispatch(addReminder(reminder)),
		editReminder: reminder => dispatch(editReminder(reminder)),
		deleteReminder: id => dispatch(deleteReminder(id))
	};
};

App.propTypes = {
	addReminder: PropTypes.func.isRequired,
	editReminder: PropTypes.func.isRequired,
	reminders: PropTypes.array.isRequired
};

export default  connect(mapStateToProps, mapDispatchToProps)(App);
