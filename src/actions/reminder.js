import { ADD_REMINDER, EDIT_REMINDER, DELETE_REMINDER } from '../constants/actionTypes';

export const  addReminder = reminder => {
	return {
		type: ADD_REMINDER,
		reminder
	};
};

export const editReminder = reminder => {
	return {
		type: EDIT_REMINDER,
		reminder 
	}
}


export const deleteReminder = id => {
	return {
		type: DELETE_REMINDER,
		id 
	}
}