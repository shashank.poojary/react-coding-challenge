const appConstants = {
	defaultColor: '#FCB900',
	descriptionMaxLength: 25,
	colors: ['#FF6900', '#FCB900', '#7BDCB5', '#00D084', '#8ED1FC', '#0693E3', '#ABB8C3', '#CDDC39', '#F78DA7', '#8BC34A']
};

export default appConstants;