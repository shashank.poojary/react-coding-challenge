import React,{Component} from 'react';
import PropTypes from 'prop-types';

import Day from '../Day';
import { filterByDay } from '../../utils/filters';
import ReminderPopover from '../ReminderPopover';

class Week extends Component {
	constructor (props) {
		super(props);
		this.renderDays = this.renderDays.bind(this);
		this.handleReminderEdit = this.handleReminderEdit.bind(this);
		this.handlePopoverToggle = this.handlePopoverToggle.bind(this);
		this.handleDeleteReminder = this.handleDeleteReminder.bind(this);
		this.renderPopover = this.renderPopover.bind(this);

		this.state = {
			isPopoverOpen: false,
			selectedReminder: undefined
		};
	}

	handleDeleteReminder (e) {
		e.stopPropagation();
		const {selectedReminder} = this.state;
		const {onDelete} = this.props;
		onDelete(selectedReminder.id);
		this.handlePopoverToggle();
	}

	handlePopoverToggle (data) {
		this.setState({isPopoverOpen: !this.state.isPopoverOpen, selectedReminder: Object.assign({}, data)});
	}

	handleReminderEdit (e) {
		e.stopPropagation();
		const {selectedReminder} = this.state;
		const {onEdit} = this.props;
		onEdit(selectedReminder);
		this.handlePopoverToggle();
	}

	renderPopover () {
		const {isPopoverOpen, selectedReminder} = this.state;
		if (isPopoverOpen) {
			return (
				<ReminderPopover 
					reminder={selectedReminder}
					onEdit={this.handleReminderEdit}
					onDelete={this.handleDeleteReminder}
					isOpen={isPopoverOpen}
					onToggle={this.handlePopoverToggle}
				/>
			);
		}
	}

	renderDays (week) {
		const {props} = this;
		return week.map(day=> 
			<Day 
				key={day} 
				date={day} 
				onDaySelect={props.onDaySelect}
				reminders={filterByDay(props.reminders, day)}
				onEdit={props.onEdit}
				onDelete={props.onDelete}
				currentDate={props.currentDate}
				onReminderSelect={this.handlePopoverToggle}
			/>
		);
	}

	render () {
		const {week} = this.props;
		return (
			<div className="calendar__week">
				{
					this.renderDays(week)
				}
				{
					this.renderPopover()
				}
			</div>
		);
	}
}

Week.propTypes = {
	week: PropTypes.array.isRequired,
	onDaySelect: PropTypes.func.isRequired,
	reminders: PropTypes.array,
	onEdit: PropTypes.func.isRequired,
	onDelete: PropTypes.func.isRequired,
	currentDate: PropTypes.object.isRequired
};

Week.defaultProps = {
	reminders: []
};

export default Week;