import React,{Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

class Toolbar extends Component {
	render () {
		const {date} = this.props;
		return (
			<div className="toolbar">
				<div className="current-month">{moment(date).format('MMM YYYY')}</div>
			</div>
		);
	}
}

Toolbar.propTypes = {
	date: PropTypes.object.isRequired
};

export default Toolbar;