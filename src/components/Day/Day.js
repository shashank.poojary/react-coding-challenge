import React,{Component} from 'react';
import PropTypes from 'prop-types';

import Reminder from '../Reminder';
import { sortByDateTime } from '../../utils/sort';
import { isDateDisabled } from '../../utils/dates';

class Day extends Component {
	constructor (props) {
		super(props);
		this.handleDayClick = this.handleDayClick.bind(this);
		this.renderReminders = this.renderReminders.bind(this);
	}

	handleDayClick () {
		const {props} = this;
		const {currentDate, date, onDaySelect} = props;
		const isDisabled = isDateDisabled(currentDate,date); 
		if (isDisabled) {
			return;
		}
		onDaySelect(props.date);
	}

	renderReminders () {
		const {reminders, onReminderSelect} = this.props;
		const sortedReminders = sortByDateTime(reminders,'date');
		return sortedReminders.map((reminder, index) => 			
			<Reminder 
				key={index}
				reminder={reminder}
				onReminderSelect={onReminderSelect}
			/>
		);
	}

	render () {
		const {date, currentDate} = this.props;
		const isDisabled = isDateDisabled(currentDate,date); 

		return (
			<div className={'calendar__day day ' + (isDisabled ? 'disabled-day' : '')} onClick={this.handleDayClick}>
				<div className='date-label'>
					{date.date()}
				</div>
				<div className='reminders-list'>
					{
						this.renderReminders()
					}
				</div>
			</div>
		);
	}
}

Day.propTypes = {
	date: PropTypes.object.isRequired,
	onDaySelect: PropTypes.func.isRequired,
	reminders: PropTypes.array,
	currentDate: PropTypes.object.isRequired,
	onReminderSelect: PropTypes.func.isRequired
};

Day.defaultProps = {
	reminders: []
};

export default Day;