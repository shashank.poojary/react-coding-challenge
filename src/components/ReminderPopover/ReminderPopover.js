import React, {Component} from 'react';
import {Popover, PopoverHeader, PopoverBody, Button} from 'reactstrap';
import PropTypes from 'prop-types';
import moment from 'moment';


class ReminderPopover extends Component {
	render () {
		const {props} = this;
		const {reminder, isOpen, onToggle, onEdit, onDelete} = props;
		return (
			<Popover 
				placement="right" 
				isOpen={isOpen} 
				target={'Popover' + reminder.id} toggle={onToggle} 
				boundariesElement='body'
			>
				<PopoverHeader style={{backgroundColor: reminder.color}}>{reminder.title === '' ? '<No Title>' : reminder.title}</PopoverHeader>
				<PopoverBody>
					{moment(reminder.date).format('ddd, MMM Do YYYY, HH:mm')} <br/>
					<div className='popover-description'>
						{reminder.description ? reminder.description : '<No description>'}
					</div>
					<br/>
					<div className="popover-footer">
						<Button color="primary" size="sm" onClick ={onEdit}>Edit</Button>{' '}
						<Button color="secondary" size="sm" onClick={onDelete}>Delete</Button>
					</div>
				</PopoverBody>
			</Popover>
		);
	}
}

ReminderPopover.propTypes = {
	reminder: PropTypes.object.isRequired,
	onEdit: PropTypes.func.isRequired,
	onDelete: PropTypes.func.isRequired,
	isOpen: PropTypes.bool.isRequired, 
	onToggle: PropTypes.func.isRequired
};

export default ReminderPopover;