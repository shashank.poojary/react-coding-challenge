import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Header from '../Header';
import Week from '../Week';
import {getWeeksOfMonth} from '../../utils/dates';
import ReminderModal from '../ReminderModal';
import appConstanst from '../../constants/appConstants';
import {fliterByWeek} from '../../utils/filters';
import {Button} from 'reactstrap';

const {defaultColor} = appConstanst;

class Month extends Component {
	constructor (props) {
		super(props);

		this.handleReminderModalToggle = this.handleReminderModalToggle.bind(this);
		this.handleAddReminder = this.handleAddReminder.bind(this);
		this.handleDaySelect = this.handleDaySelect.bind(this);
		this.handleEdit = this.handleEdit.bind(this);
		this.renderWeeks = this.renderWeeks.bind(this);
		this.renderReminderModal = this.renderReminderModal.bind(this);

		this.state = {
			isReminderModalOpen: false,
			selectedReminder: {
				description: '',
				title: '',
				date: moment(),
				color: defaultColor
			},
			editing: false
		};
	}

	handleEdit (data) {
		this.setState({selectedReminder: Object.assign({}, data), isReminderModalOpen: true, editing: true});
	}

	handleDaySelect (date) {	
		this.setState({
			selectedReminder: {
				date: moment(date),
				color: defaultColor,
				description: '',
				title: ''
			}, 
			isReminderModalOpen: true
		});
	}

	handleAddReminder (data) {
		const {editing} = this.state;
		if (editing) {
			this.props.onEditReminder(data);
		} else {
			this.props.onAddReminder(data);
		}
		this.handleReminderModalToggle();
	}

	handleReminderModalToggle () {
		this.setState(
			{ isReminderModalOpen: !this.state.isReminderModalOpen, 
				editing: false,
				selectedReminder: {
					date: moment(),
					color: defaultColor,
					description: '',
					title: ''
				}
			});	
	}

	renderWeeks (date) {
		const weeks = getWeeksOfMonth(date);
		const {reminders, onDelete} = this.props;
		return weeks.map(week => 
			<Week 
				key={week} 
				week={week}
				onDaySelect={this.handleDaySelect}
				reminders={fliterByWeek(reminders, week)}
				onEdit={this.handleEdit}
				onDelete={onDelete}
				currentDate={date}
			/>
		);
	}

	renderReminderModal () {
		const {isReminderModalOpen, selectedReminder, editing} = this.state;
		const title = editing ? 'Edit reminder' : undefined;
		if (isReminderModalOpen) {
			return (
				<ReminderModal
					isOpen={isReminderModalOpen}
					onToggle={this.handleReminderModalToggle}
					onSave={this.handleAddReminder}
					data={selectedReminder}
					titleText ={title}
				/>
			);
		}
	}

	render () {
		const {date} = this.props;
		return (
			<div>
				<div className="calendar">
					<Header />
					{
						this.renderWeeks(date)
					}
					{
						this.renderReminderModal()
					}
				</div>
				<div className="add-button float-md-right">
					<Button color="primary" size="lg" onClick={this.handleReminderModalToggle}>+</Button>
				</div>
			</div>
		);
	}
}

Month.propTypes = {
	date: PropTypes.object.isRequired,
	onAddReminder: PropTypes.func.isRequired,
	reminders: PropTypes.array.isRequired,
	onEditReminder: PropTypes.func.isRequired,
	onDelete: PropTypes.func.isRequired
};

export default Month;