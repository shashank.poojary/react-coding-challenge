import React,{Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

class Reminder extends Component {

	constructor (props) {
		super(props);
		this.handlePopoverToggle = this.handlePopoverToggle.bind(this);
	}
	
	handlePopoverToggle (e) {
		if (e) {
			e.stopPropagation();
		}
		const {reminder, onReminderSelect} = this.props;
		onReminderSelect(reminder);
	}

	render () {
		const {reminder} = this.props;
		return (
			<div 
				id={'Popover' + reminder.id}
				style={{backgroundColor: reminder.color}} 
				onClick={this.handlePopoverToggle}
				className='reminder-label'
			> 	
				{moment(reminder.date).format('HH:mm')}{' '}
				{reminder.title === '' ? 'No Title' : reminder.title}
			</div>
		);
	}
}

Reminder.propTypes = {
	reminder: PropTypes.object.isRequired,
	onReminderSelect: PropTypes.func.isRequired
};

export default Reminder;