import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Toolbar from '../Toolbar';
import MonthView from '../Month';

class Calendar extends Component {
	render () {
		const {props} = this;
		const {date, addReminder, reminders, editReminder, onDelete} = props;
		return (
			<div>
				<Toolbar 
					date={date}
				/> 
				<MonthView 
					date={date}
					onAddReminder={addReminder}
					onEditReminder={editReminder}
					reminders={reminders}
					onDelete={onDelete}
				/>
			</div>
		);
	}
}

Calendar.propTypes = {
	date: PropTypes.object.isRequired,
	reminders: PropTypes.array.isRequired,
	addReminder: PropTypes.func.isRequired,
	editReminder: PropTypes.func.isRequired,
	onDelete: PropTypes.func.isRequired
};


  
export default Calendar;