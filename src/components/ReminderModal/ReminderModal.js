import React,{Component} from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, Row, Col} from 'reactstrap';
import PropTypes from 'prop-types';
import { TwitterPicker } from 'react-color';
import moment from 'moment';

import appConstanst from '../../constants/appConstants';
const {defaultColor, descriptionMaxLength, colors} = appConstanst;


class ReminderModal extends Component {

	constructor (props) {
		super(props);
		this.handleOnChange = this.handleOnChange.bind(this);
		this.handleDateSelect = this.handleDateSelect.bind(this); 
		this.handleTimeSelect = this.handleTimeSelect.bind(this);
		this.handleColorChange = this.handleColorChange.bind(this);
		this.handleToggle = this.handleToggle.bind(this);
		this.handleOnSave = this.handleOnSave.bind(this);
		this.handleColorPickerToggle = this.handleColorPickerToggle.bind(this);
		this.renderColorPicker = this.renderColorPicker.bind(this);

		const {data} = props; 
		this.state = {
			data,
			isColorPickerOpen: false
		};
	}

	handleOnSave () {
		const {data} = this.state;
		this.props.onSave(data);
	}

	handleToggle () {
		this.setState({data: {
			title: '',
			color: defaultColor,
			description: '',
			date: null
		}});
		this.props.onToggle();
	}

	handleColorPickerToggle () {
		this.setState({isColorPickerOpen: !this.state.isColorPickerOpen});
	}

	handleColorChange (newColor) {
		const {data} = this.state;
		data.color = newColor.hex;
		this.setState({data, isColorPickerOpen: false});
	}

	handleTimeSelect (e) {
		const {value} = e.target;
		let time;
		if (value) {
			time = value.split(':');
		} else {
			time = [0,0];
		}
		const {data} = this.state;
		const newDate = data.date ? moment(data.date).toDate() : moment().toDate();
		newDate.setHours(time[0]);
		newDate.setMinutes(time[1]);
		data.date = moment(newDate);
		this.setState({data});
	}

	handleOnChange (e) {
		const {value, name} = e.target;
		const {data} = this.state;
		if (value.length > descriptionMaxLength) {
			data[name] = value.substring(0,descriptionMaxLength);
		} else {
			data[name] = value;
		}
		this.setState({data});
	}

	handleDateSelect (e) {
		const {value} = e.target;
		const {data} = this.state;
		const newDate = value ? moment(value) : moment();
		if (data.date) {
			newDate.set({hour: data.date.hour(), minute: data.date.minutes()});
		}
		data.date = newDate;
		this.setState({data});
	}

	renderColorPicker () {
		const {isColorPickerOpen} = this.state;
		if (isColorPickerOpen) {
			return (
				<TwitterPicker
					onChange={this.handleColorChange}
					colors={colors}
				/>
			);	
		}
	}

	render () {
		const {props, state} = this;
		const {isOpen, titleText} = props;
		const {title, date, description, color} = state.data;
		const isTimeDisabled = date === undefined ? true : false;
		const dateValue = isTimeDisabled ? moment().format('YYYY-MM-DD') : moment(date).format('YYYY-MM-DD');
		const timeValue = moment(date).format('HH:mm');

		return (
			<Modal isOpen={isOpen} toggle={this.handleToggle} >
				<ModalHeader toggle={this.handleToggle}>{titleText}</ModalHeader>
				<ModalBody>
					<Form>
						<FormGroup>
							<Label for="title">Title</Label>
							<Input 
								value={title}
								onChange={this.handleOnChange}
								type="text" 
								name="title" 
								id="reminder-title" 
								placeholder="Title" />
						</FormGroup>
						<FormGroup>
							<Label for="title">Date</Label>
							<Input 
								value={dateValue}
								onChange={this.handleDateSelect}
								type="date" 
								name="date" 
								id="reminder-date" />
						</FormGroup>
						<Row form>
							<Col md={8}>
								<FormGroup>
									<Label for="title">Time</Label>
									<Input 
										value={timeValue}
										type="time" 
										onChange={this.handleTimeSelect}
										disabled={isTimeDisabled}
										name="time" 
										id="reminder-timre" />
								</FormGroup>
							</Col>
							<Col md={3}>
								<FormGroup>
									<Label>Pick a color</Label>
									<Input type="button" style={{backgroundColor: color}} onClick={this.handleColorPickerToggle}/>
									{
										this.renderColorPicker()
									}
								</FormGroup>
							</Col>
						</Row>
						<FormGroup>
							<Label for="title">Description</Label>
							<Input
								value={description}
								onChange={this.handleOnChange} 
								type="textarea" 
								name="description" 
								id="reminder-desc" 
								placeholder="Description" />
							<span className='float-right'> 
								{description.length + '/' + descriptionMaxLength} 
							</span>
						</FormGroup>
					</Form>
				</ModalBody>
				<ModalFooter>
					<Button color="primary" onClick={this.handleOnSave}>Save</Button>{' '}
					<Button color="secondary" onClick={this.handleToggle}>Cancel</Button>
				</ModalFooter>
			</Modal>
		);
	}
}

ReminderModal.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	onToggle: PropTypes.func.isRequired,
	titleText: PropTypes.string,
	onSave: PropTypes.func.isRequired,
	okButtonText: PropTypes.string,
	data: PropTypes.object
};

ReminderModal.defaultProps = {
	titleText: 'Add a reminder',
	okButtonText: 'Add',
	data: {
		title: '',
		color: defaultColor,
		description: '',
		date: null
	}
};

export default ReminderModal;